package com.example.edi.services;

import com.example.edi.models.BookingBarcodeEntity;
import com.example.edi.models.BookingEntity;
import com.example.edi.models.PaymentTerm;
import com.example.edi.repositories.BookingRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BookingService {

    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    private BookingRepository bookingRepository;

    public Optional<BookingEntity> getBooking(String id) {
        return bookingRepository.findById(id);
    }

    public List<BookingEntity> getAllBookings() {
        return bookingRepository.findAll();
    }

    @Transactional
    public BookingEntity createNewBooking(String requestIdentifier, String platformId, String platformName,
                                          String documentType, String paymentTerm, String base64Barcode) {

        Optional<BookingEntity> entityCheck = getBooking(requestIdentifier);
        if(entityCheck.isPresent()) {
            throw new RuntimeException("Entity already exist");
        }

        BookingEntity entity = new BookingEntity();
        entity.setId(requestIdentifier);
        entity.setPlatformId(platformId);
        entity.setPlatformName(platformName);
        entity.setDocumentType(documentType);
        entity.setPaymentTerm(PaymentTerm.valueOf(paymentTerm));

        BookingBarcodeEntity barcode = new BookingBarcodeEntity();
        barcode.setId(requestIdentifier);
        barcode.setDescription(base64ToByteArray(base64Barcode));
        entity.setBarcode(barcode);

        return bookingRepository.save(entity);
    }

    private byte[] base64ToByteArray(String base64String) {
        try {
            byte[] bytes = base64String.getBytes("UTF-8");
            String encoded = Base64.getEncoder().encodeToString(bytes);
            return Base64.getDecoder().decode(encoded);
        } catch (Exception ex) {
            throw new RuntimeException("Unable to convert to Byte array!", ex);
        }
    }

    @Transactional
    public void deleteBooking(String id) {

        Optional<BookingEntity> entityCheck = getBooking(id);
        if(!entityCheck.isPresent()) {
            throw new RuntimeException("Entity does not exist");
        }

        bookingRepository.delete(entityCheck.get());
    }


    public static void main(String[] args) {
        String input = "Hello World";
        String encodedString = Base64.getEncoder().encodeToString(input.getBytes());
        System.out.println(encodedString);
    }
}
