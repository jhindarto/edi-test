package com.example.edi.models;

import javax.persistence.*;

@Entity(name = "log_johan_api01")
public class BookingEntity {

    @Id
    @Column(name = "idRequestBooking", length = 255)
    private String id;

    @Column(name = "id_platform", length = 20)
    private String platformId;

    @Column(name = "nama_platform", length = 20)
    private String platformName;

    @Column(name = "doc_type", length = 20)
    private String documentType;

    @Enumerated(EnumType.STRING)
    @Column(name = "term_of_payment", length = 5)
    private PaymentTerm paymentTerm;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private BookingBarcodeEntity barcode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public BookingBarcodeEntity getBarcode() {
        return barcode;
    }

    public void setBarcode(BookingBarcodeEntity barcode) {
        this.barcode = barcode;
    }
}
