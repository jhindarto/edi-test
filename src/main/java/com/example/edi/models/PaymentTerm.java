package com.example.edi.models;

public enum PaymentTerm {

    CBS("Cash before shipment"),
    CIA("Cash in advance");

    private String description;

    private PaymentTerm(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
