package com.example.edi.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity(name = "images_johan_api01")
public class BookingBarcodeEntity {

    @Id
    @Column(name = "idRequestBooking", length = 255)
    private String id;

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getDescription() {
        return description;
    }

    public void setDescription(byte[] description) {
        this.description = description;
    }
}
