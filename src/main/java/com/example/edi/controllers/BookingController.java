package com.example.edi.controllers;

import com.example.edi.models.BookingEntity;
import com.example.edi.models.ResponseMessage;
import com.example.edi.services.BookingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/booking")
public class BookingController {

    private static Logger LOGGER = LogManager.getLogger();

    @Autowired
    private BookingService bookingService;

    // get specific booking by uuid
    @GetMapping("/{id}")
    public ResponseEntity getSpecificBooking(@PathVariable String id) {

        LOGGER.info("GET SPECIFIC Booking with id: " + id);
        Optional<BookingEntity> result = bookingService.getBooking(id);
        if(result.isPresent()) {
            return ResponseEntity.ok(result.get());
        }
        return ResponseEntity.noContent().build();
    }

    // get all booking
    @GetMapping("/")
    public ResponseEntity getAllBookings() {

        LOGGER.info("GET ALL BOOKING");
        List<BookingEntity> result = bookingService.getAllBookings();
        if(!result.isEmpty()) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.noContent().build();
    }

    // create booking
    @PostMapping("/")
    public ResponseEntity createNewBooking(String requestIdentifier, String platformId, String platformName,
                                           String documentType, String paymentTerm, String base64Barcode) {

        LOGGER.info("CREATE NEW BOOKING");
        BookingEntity result = bookingService.createNewBooking(requestIdentifier, platformId, platformName,
                documentType, paymentTerm, base64Barcode);
        if(result != null) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ResponseMessage(false, "There is technical issue"));
    }

    // update booking
    @PutMapping("/{id}")
    public ResponseEntity updateBooking(@PathVariable String id) {

        LOGGER.info("Update BOOKING: " + id);
        return ResponseEntity.ok(new ResponseMessage(false, "Not Implemented"));
    }

    // delete booking
    @DeleteMapping("/{id}")
    public ResponseEntity deleteBooking(@PathVariable String id) {

        LOGGER.info("Delete BOOKING: " + id);
        try {
            bookingService.deleteBooking(id);
            return ResponseEntity.ok(new ResponseMessage(true, String.format("Booking %s is deleted", id)));
        } catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ResponseMessage(false, "There is technical issue"));
        }
    }

}
